let colorlist = ['gold', 'yellow', 'turquoise', 'red'];

const pedalParado = 0;
const pedalIndoParaCima = 1;
const pedalIndoParaBaixo = 2;

const pedalEsquerdo = 0;
const pedalDireito = 1;
const teto = 2;
const chao = 3;
const bola = 4;

const posicaoX = 0;
const posicaoY = 1;
const largura = 2;
const altura = 3;

let velocidadesBola = [10.0,0.0];

let movimentoPedalEsquerdo;
let movimentoPedalDireito;

let objetos;
let velocidadePedal;

function setup() {
  createCanvas(windowWidth, windowHeight);
  objetos = [
    [0, windowHeight / 2 - (100 / 2), 30, 100],
    [windowWidth - 30, windowHeight / 2 - (100 / 2), 30, 100],
    [0, 0, windowWidth, 10],
    [0, windowHeight - 10, windowWidth, 10],
    [windowWidth / 2 - 30, windowHeight / 2 - 30, 30, 30],
  ];
  velocidadePedal = 10;
  movimentoPedalDireito = pedalParado;
  movimentoPedalEsquerdo = pedalParado;
  background(0);
}

function draw() {
  background(0);
  
  update();
  desenhar_objetos();
  
}

function desenhar_objetos(){
  objetos.forEach((obj) => {
    rect(obj[0], obj[1], obj[2], obj[3]);
  })
  
}

function update() {
 
  if(keyIsDown(UP_ARROW)){
    objetos[pedalEsquerdo][posicaoY] -= velocidadePedal;
    movimentoPedalEsquerdo = pedalIndoParaCima;
  } else if(keyIsDown(DOWN_ARROW)) {
    objetos[pedalEsquerdo][posicaoY] += velocidadePedal;
    movimentoPedalEsquerdo = pedalIndoParaBaixo;
  } else if(keyIsDown("W".charCodeAt(0))) {
    objetos[pedalDireito][posicaoY] -= velocidadePedal;
    movimentoPedalDireito = pedalIndoParaCima;
  } else if(keyIsDown("S".charCodeAt(0))) {
    objetos[pedalDireito][posicaoY] += velocidadePedal;
    movimentoPedalDireito = pedalIndoParaBaixo;
  }else {
    movimentoPedalDireito = pedalParado;
    movimentoPedalEsquerdo = pedalParado;
  }
      
  if (objetos[pedalEsquerdo][posicaoY] < objetos[teto][posicaoY] + objetos[teto][altura]){
    objetos[pedalEsquerdo][posicaoY] += objetos[teto][posicaoY] + objetos[teto][altura] - objetos[pedalEsquerdo][posicaoY];
  }
  
  if (objetos[pedalEsquerdo][posicaoY] + objetos[pedalEsquerdo][altura] > objetos[chao][posicaoY] - objetos[chao][altura]){
    objetos[pedalEsquerdo][posicaoY] = objetos[chao][posicaoY] - objetos[pedalEsquerdo][altura];
  }
  
  if (objetos[pedalDireito][posicaoY] < objetos[teto][posicaoY] + objetos[teto][altura]){
    objetos[pedalDireito][posicaoY] += objetos[teto][posicaoY] + objetos[teto][altura] - objetos[pedalDireito][posicaoY];
  }
  
  if (objetos[pedalDireito][posicaoY] + objetos[pedalDireito][altura] > objetos[chao][posicaoY] - objetos[chao][altura]){
    objetos[pedalDireito][posicaoY] = objetos[chao][posicaoY] - objetos[pedalDireito][altura];
  }

  objetos.forEach((obj, indice) => {
    if(indice != bola && bolaColidiu(obj)) {
      if (indice == teto || indice == chao) {
          velocidadesBola[posicaoY] *= -1;
      } else if (indice == pedalEsquerdo) {
        if (movimentoPedalEsquerdo == pedalParado) {
          velocidadesBola[posicaoX] *= -1;
        } else if (movimentoPedalEsquerdo == pedalIndoParaBaixo) {
          velocidadesBola[posicaoY] = velocidadesBola[posicaoY] == 0 ?  velocidadesBola[posicaoY] + 10 : velocidadesBola[posicaoY];
          velocidadesBola[posicaoY] = velocidadesBola[posicaoY] > 0 ? velocidadesBola[posicaoY] : velocidadesBola[posicaoY] * -1;
          velocidadesBola[posicaoX] *= -1;
        } else if (movimentoPedalEsquerdo == pedalIndoParaCima) {
          velocidadesBola[posicaoY] = velocidadesBola[posicaoY] == 0 ?  velocidadesBola[posicaoY] + 10 : velocidadesBola[posicaoY];
          velocidadesBola[posicaoY] = velocidadesBola[posicaoY] < 0 ? velocidadesBola[posicaoY] : velocidadesBola[posicaoY] * -1;
          velocidadesBola[posicaoX] *= -1;
        }
      } else if (indice == pedalDireito) {
        if (movimentoPedalDireito == pedalParado) {
          velocidadesBola[posicaoX] *= -1;
        } else if (movimentoPedalDireito == pedalIndoParaBaixo) {
          velocidadesBola[posicaoY] = velocidadesBola[posicaoY] == 0 ?  velocidadesBola[posicaoY] + 10 : velocidadesBola[posicaoY];
          velocidadesBola[posicaoY] = velocidadesBola[posicaoY] > 0 ? velocidadesBola[posicaoY] : velocidadesBola[posicaoY] * -1;
          velocidadesBola[posicaoX] *= -1;
        } else if (movimentoPedalDireito == pedalIndoParaCima) {
          velocidadesBola[posicaoY] = velocidadesBola[posicaoY] == 0 ?  velocidadesBola[posicaoY] + 10 : velocidadesBola[posicaoY];
          velocidadesBola[posicaoY] = velocidadesBola[posicaoY] < 0 ? velocidadesBola[posicaoY] : velocidadesBola[posicaoY] * -1;
          velocidadesBola[posicaoX] *= -1;
        }
      }
    }
  });

  objetos[bola][posicaoX] += velocidadesBola[posicaoX];
  objetos[bola][posicaoY] += velocidadesBola[posicaoY];
  
}

function bolaColidiu(obj) {
  
  if (
      objetos[bola][posicaoX] > obj[posicaoX] && objetos[bola][posicaoX] < obj[posicaoX] + obj[largura] &&
      objetos[bola][posicaoY] > obj[posicaoY] && objetos[bola][posicaoY] < obj[posicaoY] + obj[altura] ||
      objetos[bola][posicaoX] + objetos[bola][largura] > obj[posicaoX] && objetos[bola][posicaoX] + objetos[bola][largura] < obj[posicaoX] + obj[largura] &&
      objetos[bola][posicaoY] > obj[posicaoY] && objetos[bola][posicaoY] < obj[posicaoY] + obj[altura] ||
      objetos[bola][posicaoX] > obj[posicaoX] && objetos[bola][posicaoX] < obj[posicaoX] + obj[largura] &&
      objetos[bola][posicaoY] + objetos[bola][altura] > obj[posicaoY] &&  obj[posicaoY] + obj[altura]  > objetos[bola][posicaoY] + objetos[bola][altura] ||
      objetos[bola][posicaoX] + objetos[bola][largura] > obj[posicaoX] && objetos[bola][posicaoX] + objetos[bola][largura] < obj[posicaoX] + obj[largura] &&
      objetos[bola][posicaoY] + objetos[bola][altura] > obj[posicaoY] &&  objetos[bola][posicaoY] + objetos[bola][altura] < obj[posicaoY] + obj[altura]
  ) {
    return true;
  }
  return false;
}